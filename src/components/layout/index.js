import React from 'react'
import { connect } from "react-redux"
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { Link } from 'gatsby'

import './layout.css'

const Header = ({ userEmail, userID }) => (
  <div
    style={{
      background: 'gray',
      marginBottom: '75px',
    }}
  >
    <div
      style={{
        margin: '0 auto',
        maxWidth: 960,
        padding: '1.45rem 1.0875rem',
      }}
    >
      <h1 style={{ 
        margin: 0,
        display:'flex',
        justifyContent:'space-between',
        alignItems: 'flex-end',
      }}>
        <Link
          to="/"
          style={{
            color: 'white',
            textDecoration: 'none',
          }}
        >
          Gatsby
        </Link>
        <Link
          to="/user"
          style={{
            color: 'white',
            textDecoration: 'none',
            fontSize:'24px',
          }}
        >
          Login
        </Link>
        {userID && 
          <Link
          to={`user/${userID}`}
          style={{
            color: 'white',
            textDecoration: 'none',
            fontSize:'24px',
          }}
        >
        User Data
        </Link>
        }
        {userEmail && <p style={{fontSize: '16px'}}> Logged as {userEmail} </p> }
        <Link
          to="/blog"
          style={{
            color: 'white',
            textDecoration: 'none',
            fontSize:'24px',
          }}
        >
          Blog
        </Link>
      </h1>
    </div>
  </div>
)

const Layout = ({ children }) => (
  <div>
    <Helmet
      title='Gatsby'
      meta={[
        { name: 'description', content: 'Sample' },
        { name: 'keywords', content: 'sample, something' },
      ]}
    />
    <ConnectedHeader />
    <div
      style={{
        margin: '0 auto',
        maxWidth: 960,
        padding: '0px 1.0875rem 1.45rem',
        paddingTop: 0,
      }}
    >
      {children}
    </div>
  </div>
)

const mapStateToProps = ({ userEmail, userID }) => {
  return { userEmail, userID }
}

const ConnectedHeader = connect(
  mapStateToProps,
)(Header);


Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
