import React, { Component } from 'react';
import { connect } from "react-redux";
import { REST_API_ENDPOINT } from '../../common/consts';
import Layout from '../../components/layout';

class TodoList extends Component {
  state={
    todos: [],
    properID: false,
    isLoaded: false,
}

async componentDidMount() {
  const { userID, userId } = this.props;
  const properID = userID === userId;
  if(properID) {
    const data = await fetch(`${REST_API_ENDPOINT}/todos`).then(resp => resp.json());
    this.setState({
        todos: data.todos,
    })
  }
  this.setState({
    properID,
    isLoaded: true,
})
}

  render() {
    const { todos, isLoaded } = this.state;

    return (
      <Layout>
        {isLoaded && (this.state.properID ?
        <div>
          {todos.map(todo => (
            <div>
              <h3>{todo.title}</h3>
              <p>{todo.text}</p>
            </div>
          ))}
        </div> : <p> Incorrect user ID </p> )}
      </Layout>
    )
  }
}

const mapStateToProps = ({ userID }) => {
  return { userID }
}

const ConnectedTodoList = connect(
  mapStateToProps,
)(TodoList);

export default ConnectedTodoList;