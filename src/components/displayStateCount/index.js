import React from 'react'
import { connect } from "react-redux"

import styles from './index.module.css';

const DisplayState = ({ count }) => (
  <div>
    <p className={styles.Paragraph}>Redux state count: {count}</p>
  </div>
)

const mapStateToProps = ({ count }) => {
  return { count }
}

const DisplayStateValue = connect(
  mapStateToProps,
)(DisplayState);

export default DisplayStateValue;