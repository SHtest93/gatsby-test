import React, { Component } from 'react'
import { connect } from "react-redux"
import Layout from '../../components/layout'

import Form from './formComponent'

const mapDispatchToProps = dispatch => {
  return { 
   login: (token, user, userID) => dispatch({ type: `USER_LOGGED_IN`, token, user, userID }),
   }
}

export const ConnectedForm = connect(
  null,
  mapDispatchToProps
)(Form)

export default class AnotherPage extends Component {
  render() {
    return (
      <Layout>
        <ConnectedForm/>
      </Layout>
    )
  }
}
