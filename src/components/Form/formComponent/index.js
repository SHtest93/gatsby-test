import React, { Component } from 'react'

import { REST_API_ENDPOINT } from '../../../common/consts';

import styles from './form.module.css'
import { navigate } from "@reach/router"

export default class Form extends Component {
    constructor(props) {
        super(props);    
        this.handleInputChange = this.handleInputChange.bind(this);
        this.userLogin = this.userLogin.bind(this);
       // this.userSignIn = this.userSignIn.bind(this);
      }

      state={
        email: '',
        password: '',
        loginResultMessage: '',
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;
      
        this.setState({
          [name]: value
        });
      }
      
      async userLogin(event) {
          event.preventDefault();
          const {
            state: {
              email,
              password,
            }
          } = this;
      
          const response = await fetch(`${REST_API_ENDPOINT}/user/login`, {
            method: 'POST',
            body: JSON.stringify({ email, password }),
            headers: {
              'Content-Type': 'application/json',
            },
          }).then(res => res.json())

          this.props.login(response.token, response.user, response.userID)

          if(response.token) {
            navigate(`/user/${response.userID}`);
          }

          this.setState({
              loginResultMessage: response.message,
              email: '',
              password: '',
            });
        }
     
        /*
      async userSignIn(event) {
          event.preventDefault();
          const {
            state: {
              email,
              password,
            }
          } = this;
      }
      */

  render() {
    const { loginResultMessage } = this.state;

    return (
      <div className={styles.FormBox}>
        <form onSubmit={this.userSignIn}>
            <input
                type="text"
                name="email"
                value={this.state.email}
                required
                placeholder="Email"
                onChange={this.handleInputChange}
                className={styles.Input}
            />
            <input
                type="password"
                name="password"
                value={this.state.password}
                required
                placeholder="Password"
                onChange={this.handleInputChange}
                className={styles.Input}
            />
           {/* <button className={styles.Button} type='submit' name='signIn'> SignIn </button> */}
            <button className={styles.Button} name='login' onClick={this.userLogin}> Login </button>
            {loginResultMessage && <p>{loginResultMessage}</p>}
            </form>
      </div>
    )
  }
}
