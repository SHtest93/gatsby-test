import React from 'react'
import { Link } from 'gatsby'
import _ from 'lodash'
import Layout from '../components/layout'
import styles from './blog-list.module.css';

import DisplayReduxState from '../components/displayStateCount';

const BlogPost = ({ node }) => {
  return (
    <div className={styles.BlogItemBox}>
      <Link to={`/blog/${node.slug}`}>
        <img className={styles.BlogImage} src={node.heroImage.file.url} />
        <h3 className={styles.BlogTitle}>{node.title}</h3>
      </Link>
    </div>
  )
}

const IndexPage = ({ data, pageContext }) => {
const { numPages, currentPage } = pageContext;

return (
  <Layout>
    <DisplayReduxState />
    {_.times(numPages, i => { 
      const isActive = i+1 === currentPage;
      if(i === 0) {
        return <Link to='blog' style={{marginRight:'15px', color:isActive ? 'red' : 'black'}}> {i+1} </Link>
      } else {
        return <Link to={`/blog/${i+1}`} style={{marginRight:'15px', color:isActive ? 'red' : 'black'}}> {i+1} </Link> 
      }})}
    <div className={styles.ContentBox}>
      {data.allContentfulBlogPost.edges.map((edge,item) => <BlogPost key={item} node={edge.node} />)}
    </div>
  </Layout>
)}

export default IndexPage

export const blogListQuery = graphql`
  query blogListQuery($skip: Int!, $limit: Int!) {
    allContentfulBlogPost(
      sort: { fields: [publishDate], order: DESC }
      limit: $limit
      skip: $skip
    ) {
      edges {
        node {
            publishDate
            title
            slug
            heroImage {
              file {
                url
              }
            }
          }
      }
    }
  }
`;