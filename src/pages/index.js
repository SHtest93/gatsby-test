import React from 'react'
import { connect } from "react-redux"

import DisplayReduxState from '../components/displayStateCount';
import Layout from '../components/layout'

const Counter = ({ increment, decrement }) => (
  <div>
    <DisplayReduxState />
    <button onClick={decrement}>Decrement</button>
    <button onClick={increment}>Increment</button>
  </div>
)

const mapDispatchToProps = dispatch => {
  return { 
    increment: () => dispatch({ type: `INCREMENT` }),
    decrement: () => dispatch({ type: `DECREMENT` })
   }
}

const ConnectedCounter = connect(
  null,
  mapDispatchToProps
)(Counter)


const HomePage = () => (
  <Layout>
    <h1>Homepage</h1>
    <br/>
    <ConnectedCounter />
  </Layout>
)

export default HomePage;