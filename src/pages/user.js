import React from 'react';
import { Router } from "@reach/router"
import TodoList from '../components/TodoList';
import Form from '../components/Form';

const User = () => {
  return (
    <Router>
        <Form 
            exact
            path="/user"
        />
        <TodoList
            path="/user/:userId/"
        />
    </Router>
     );
  }

export default User;