import { createStore as reduxCreateStore } from "redux"

const reducer = (state, action) => {
  if (action.type === `INCREMENT`) {
    return Object.assign({}, state, {
      count: state.count + 1,
    })
  }
  if (action.type === `DECREMENT`) {
    return Object.assign({}, state, {
      count: state.count - 1,
    })
  }
  if (action.type === `USER_LOGGED_IN`) {
    console.log(action)
    return Object.assign({}, state, {
      userToken: action.token,
      userEmail: action.user,
      userID: action.userID,
    })
  }
  return state;
}

const initialState = { count: 0, userToken: '', userEmail: '', userID: '', }

const createStore = () => reduxCreateStore(reducer, initialState);

export default createStore;